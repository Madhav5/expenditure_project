from django import forms
from .models import *
from django.contrib.auth.models import User




class ExpenditureForm(forms.ModelForm):
	class Meta:
		model=CustomerExpenditure
		fields=['food','education','cloths','vehicle','extra','saving','status']
		widgets={
		'food': forms.NumberInput(attrs={
                "class": "form-control",
                'placeholder':'Enter food amount...'
            }),
		'education': forms.NumberInput(attrs={
                "class": "form-control",
                'placeholder':'Enter education amount...'
            }),
		'cloths': forms.NumberInput(attrs={
                "class": "form-control",
                'placeholder':'Enter cloths amount...'
            }),
		'vehicle': forms.NumberInput(attrs={
                "class": "form-control",
                'placeholder':'Enter vehicle amount...'
            }),
		'extra': forms.NumberInput(attrs={
                "class": "form-control",
                'placeholder':'Enter extra amount...'
            }),
		'saving': forms.NumberInput(attrs={
                "class": "form-control",
                'placeholder':'Enter saving amount...'
            }),
		'status': forms.Select(attrs={
                "class": "form-control",
               
            }),
		}



class LoginForm(forms.Form):
    email = forms.CharField(widget=forms.EmailInput(attrs={
        "class": "form-control",
        "autocomplete": "off",
        "placeholder": "Enter Email"
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': "form-control",
        "autocomplete": "off",
        "placeholder": "Enter password"
    }))




class SignupForm(forms.Form):
	username=forms.CharField(widget=forms.TextInput(attrs={
		"class":"form-control",
		"autocomplete":"off",
		"placeholder":"Enter Username"
		}))
	email=forms.EmailField(widget=forms.EmailInput(attrs={
		"class":"form-control",
		"autocomplete":"off",
		"placeholder":"Enter email"
		}))
	password=forms.CharField(widget=forms.PasswordInput(attrs={
		"class":"form-control",
		"autocomplete":"off",
		"placeholder":"Enter Password"
		}))
	confirm_password=forms.CharField(widget=forms.PasswordInput(attrs={
		"class":"form-control",
		"autocomplete":"off",
		"placeholder":"Enter Password"
		}))


	def clean_username(self):
		uname=self.cleaned_data['username']
		if User.objects.filter(username=uname).exists():
			raise forms.ValidationError("Username is already exist")

		
		return uname



	def clean_confirm_password(self):
		pw=self.cleaned_data['password']
		c_pw=self.cleaned_data['confirm_password']
		if (pw !=c_pw):
			raise forms.ValidationError("Password did not match.")

		if (len(pw)<6):
			raise forms.ValidationError("Password too short")

		numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
		c = 0
		for i in c_pw:
			if i in numbers:
				c += 1
		if c < 1:
			raise forms.ValidationError("must include a number")


		return c_pw



class CategoryForm(forms.ModelForm):
	class Meta:
		model=Category
		fields=['name']




class SubjectForm(forms.ModelForm):
	class Meta:
		model=Subject
		fields=['category','title_name','amount']

	def __init__(self, user, *args, **kwargs):
		super(SubjectForm, self).__init__(*args, **kwargs)
		self.fields['category'].queryset = Category.objects.filter(person=user)

