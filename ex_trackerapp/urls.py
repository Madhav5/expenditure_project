from django.urls import path
from .views import *



app_name="ex_trackerapp"
urlpatterns=[
    path('',ClientExpenditureList.as_view(),name='clientexlist'),	
	path("expenditure/",ClientExpenditureView.as_view(),name='clientexpenditure'),
	path("client/expn-<int:pk>/detail/",ClientExpenDetailView.as_view(),name='clientexpendetail'),

	path('pie-chart/',pie_chart, name='pie-chart'),
	path('barhome/',home, name='home'),
    path('expenditure-chart/',expenditure_chart, name='expenditure-chart'),
    

    path('clientexpenselist2/',ClientExpenditureList2.as_view(),name='clientexlist2'),
	path("client/expn-<int:pk>/detail2/",ClientExpenDetailView2.as_view(),name='clientexpendetail2'),

	path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("register/", SignupView.as_view(), name="signup"),
    path("ajax_check_useremail/", AjaxCheckUseremailView.as_view(),name='ajaxcheckuseremail'),

	path("self-expenditure/",ClientSelfExpenditureView.as_view(),name='clientselfexpenditure'),
	path("self-expenditure2/",ClientSelfExpenditureView2.as_view(),name='clientselfexpenditure2'),
	








	]
