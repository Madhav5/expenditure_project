
from django.shortcuts import render, redirect


from django.views.generic import *
from .models import *
from .forms import *
from django.db.models import Sum
from django.http import JsonResponse
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse, reverse_lazy




class LoginRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            pass
        else:
            return redirect("ex_trackerapp:login")

        return super().dispatch(request, *args, **kwargs)



class DetailPageMixin(object):
    def dispatch(self, request, *args, **kwargs):
        itm_id=self.kwargs["pk"]
        this_item=CustomerExpenditure.objects.get(id=itm_id)
        if this_item.usr==request.user:
            pass
        else:
            return render(request, 'clienttemplates/clienterrorpage.html')

        return super().dispatch(request, *args, **kwargs)



def pie_chart(request):
    labels = []
    data = []

    queryset = Subject.objects.filter(person=request.user).order_by('-amount')[:5]
    for sp in queryset:
        labels.append(sp.title_name)
        data.append(sp.amount)

    return render(request, 'clienttemplates/clientpie_chart.html', {
        'labels': labels,
        'data': data,
    })



def home(request):
    return render(request, 'clienttemplates/clientbarhome.html')

def expenditure_chart(request):
    labels = []
    data = []

    queryset = Subject.objects.filter(person=request.user).values('category__name').annotate(category_amount=Sum('amount')).order_by('-category_amount')
    for entry in queryset:
        labels.append(entry['category__name'])
        data.append(entry['category_amount'])
    
    return JsonResponse(data={
        'labels': labels,
        'data': data,
    })





class ClientExpenditureList(LoginRequiredMixin,ListView):
    template_name='clienttemplates/clienthome.html'
    queryset=CustomerExpenditure.objects.all().order_by('-id')
    context_object_name="allexpenses"

    



class ClientExpenDetailView( DetailPageMixin,DetailView):
    template_name='clienttemplates/clientexpendetail.html'
    model=CustomerExpenditure
    context_object_name="expenditure"

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        itm_id=self.kwargs["pk"]
        this_item=CustomerExpenditure.objects.get(id=itm_id)
        a=this_item.food
        b=this_item.education
        c=this_item.cloths
        d=this_item.vehicle
        e=this_item.extra
        f=this_item.saving
        context={
        'labels':['Food','Education','Cloths','Vehicle','Extra','Saving'],
        'data':[a,b,c,d,e,f]
        }
         

        return context



class ClientExpenditureList2(LoginRequiredMixin,ListView):
    template_name='clienttemplates/clienthome2.html'
    queryset=CustomerExpenditure.objects.all().order_by('-id')
    context_object_name="allexpenses2"



class ClientExpenDetailView2(DetailPageMixin, DetailView):
    template_name='clienttemplates/clientexpendetail2.html'
    model=CustomerExpenditure
    context_object_name="expenditure2"

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        itm_id=self.kwargs["pk"]
        this_item=CustomerExpenditure.objects.get(id=itm_id)
        a=this_item.food
        b=this_item.education
        c=this_item.cloths
        d=this_item.vehicle
        e=this_item.extra
        f=this_item.saving
        context={
        'labels':['Food','Education','Cloths','Vehicle','Extra','Saving'],
        'data':[a,b,c,d,e,f]
        }
         

        return context




class LoginView(FormView):
    template_name = "clienttemplates/login.html"
    form_class = LoginForm
    success_url = reverse_lazy('ex_trackerapp:clientexlist')

    def form_valid(self, form):
        x = form.cleaned_data["email"]
        y = form.cleaned_data["password"]
        usr = authenticate(username=x, password=y)
        if usr is not None:
            login(self.request, usr)

        else:
            return render(self.request, 'clienttemplates/login.html',
                          {
                              "error": "Invalid Credential",
                              "form": form
                          })

        return super().form_valid(form)

  

class LogoutView(View):
    def get(self, request):
        logout(request)

        return redirect('/')


class SignupView(FormView):
    template_name = "clienttemplates/signup.html"
    form_class = SignupForm
    success_url = "/"

    def form_valid(self, form):
        x = form.cleaned_data["username"]
        e = form.cleaned_data["email"]
        y = form.cleaned_data["password"]

        usr = User.objects.create_user(e, e, y)
        login(self.request, usr)

        return super().form_valid(form)


class AjaxCheckUseremailView(View):
    def get(self, request, *args, **kwargs):
        u = request.GET['email']
        if User.objects.filter(email=u).exists():
            message = "unavailable"

        else:
            message = "available"

        return JsonResponse({
            "message": message
        })




class ClientExpenditureView(LoginRequiredMixin,CreateView):
    template_name='clienttemplates/clientexpenditure.html'
    form_class=ExpenditureForm
    success_url=reverse_lazy('ex_trackerapp:clientexlist')

    def form_valid(self, form):
        creator=self.request.user
        form.instance.usr=creator

        return super().form_valid(form)



class ClientSelfExpenditureView(CreateView):
    template_name='clienttemplates/clientselfexpenditurecategory.html'
    form_class=SubjectForm
    success_url=reverse_lazy('ex_trackerapp:clientselfexpenditure')


    # def get_context_data(self,**kwargs):
    #     context=super().get_context_data(**kwargs)
    #     context['abc']=SubjectForm
        
    #     return context

    def get_form_kwargs(self):
        kwargs = super(ClientSelfExpenditureView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        creator=self.request.user
        form.instance.person=creator

        return super().form_valid(form)



class ClientSelfExpenditureView2(CreateView):
    template_name='clienttemplates/clientselfexpenditurecategory2.html'
    form_class=CategoryForm
    success_url=reverse_lazy('ex_trackerapp:clientselfexpenditure')

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        context['def']=CategoryForm
        
        return context

    def form_valid(self, form):
        creator=self.request.user
        form.instance.person=creator

        return super().form_valid(form)


    



