from django.apps import AppConfig


class ExTrackerappConfig(AppConfig):
    name = 'ex_trackerapp'
