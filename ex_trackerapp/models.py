from django.db import models
from .constants import *
from django.contrib.auth.models import User

# Create your models here.
class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True



class CustomerExpenditure(TimeStamp):
    usr=models.ForeignKey(User,on_delete=models.CASCADE)
    food=models.PositiveIntegerField()
    education=models.PositiveIntegerField()
    cloths=models.PositiveIntegerField()
    vehicle=models.PositiveIntegerField()
    extra=models.PositiveIntegerField()
    saving=models.PositiveIntegerField()
    status=models.CharField(max_length=50,choices=STATUS)

    def __str__(self):
        return self.usr


class Category(models.Model):
    person=models.ForeignKey(User,on_delete=models.CASCADE)
    name = models.CharField(max_length=30,unique=False)

    def __str__(self):
    	return self.name

class Subject(models.Model):
    person=models.ForeignKey(User,on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    title_name= models.CharField(max_length=30)
    amount = models.PositiveIntegerField()

    def __str__(self):
        return self.title_name





