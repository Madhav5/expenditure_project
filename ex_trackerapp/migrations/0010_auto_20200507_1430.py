# Generated by Django 3.0.4 on 2020-05-07 08:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ex_trackerapp', '0009_auto_20200502_1527'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(max_length=30),
        ),
    ]
